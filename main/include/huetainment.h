#pragma once

#include <QObject>
#include <QStringListModel>
#include <QReadWriteLock>
#include <QAtomicInteger>
#include <QNetworkAccessManager>
#include <QMutex>
#include <QSize>

#include <memory>

#include "capture.h"

class BridgeDiscovery;
class EntertainmentGroup;

extern QNetworkAccessManager* qnam;

class Huetainment : public QObject
{
  Q_OBJECT

  Q_PROPERTY(BridgeDiscovery* bridgeDiscovery READ getBridgeDiscovery NOTIFY hueInit)
  Q_PROPERTY(QList<QObject*> entertainmentGroupsModel READ getEntertainmentGroupsModel NOTIFY entertainmentGroupsChanged)
  Q_PROPERTY(bool syncing MEMBER m_syncing NOTIFY syncingChanged)
  Q_PROPERTY(bool capturing MEMBER m_capturing NOTIFY capturingChanged)

  Q_PROPERTY(int lastFrameReadElapsed READ getLastFrameReadElapsed NOTIFY frameReadElapsedChanged)
  Q_PROPERTY(int messageSendElapsed READ getMessageSendElapsed NOTIFY messageSendElapsedChanged)

  Q_PROPERTY(QString syncingGroup MEMBER m_syncingGroup);
  Q_PROPERTY(int captureInterval READ getCaptureInterval WRITE setCaptureInterval NOTIFY captureParamsChanged)

  Q_PROPERTY(double minLuminance READ getMinLuminance WRITE setMinLuminance NOTIFY syncParamsChanged)
  Q_PROPERTY(double maxLuminance READ getMaxLuminance WRITE setMaxLuminance NOTIFY syncParamsChanged)
  Q_PROPERTY(double chromaBoost READ getChromaBoost WRITE setChromaBoost NOTIFY syncParamsChanged)
  Q_PROPERTY(double lumaBoost READ getLumaBoost WRITE setLumaBoost NOTIFY syncParamsChanged)
  Q_PROPERTY(double colorBias READ getLumaBoost WRITE setLumaBoost NOTIFY syncParamsChanged)

  Q_PROPERTY(double centerSlowness READ getCenterSlowness WRITE setCenterSlowness NOTIFY syncParamsChanged)
  Q_PROPERTY(double sideSlowness READ getSideSlowness WRITE setSideSlowness NOTIFY syncParamsChanged)

  Q_PROPERTY(double satThreshold READ getSatThreshold WRITE setSatThreshold NOTIFY syncParamsChanged)
  Q_PROPERTY(double valThreshold READ getValThreshold WRITE setValThreshold NOTIFY syncParamsChanged)

  Q_PROPERTY(QString captureDevice MEMBER m_captureDevice);
  Q_PROPERTY(QString captureVideoStandard MEMBER m_captureVideoStandard);
  Q_PROPERTY(QRect captureMargins MEMBER m_captureMargins);
  Q_PROPERTY(int captureMultiplier MEMBER m_captureMultiplier);

public:
  explicit Huetainment(QObject *parent = nullptr);
  virtual ~Huetainment();

  void ReadSettings();
  void WriteSettings();
  Q_INVOKABLE void ResetSettings();

  BridgeDiscovery* getBridgeDiscovery() {
    return bridgeDiscovery;
  }
  QList<QObject*> getEntertainmentGroupsModel() {
    return *(reinterpret_cast<QList<QObject*>*>(&entertainmentGroups));
  }

  int getMessageSendElapsed();

  int getCaptureInterval() {
    return captureInterval;
  }

  void setCaptureInterval(int interval);

  double getMinLuminance() {
    return minLuminance / 1000.0;
  }

  void setMinLuminance(double in) {
    minLuminance = in * 1000.0;
    emit syncParamsChanged();
  }

  double getMaxLuminance() {
    return maxLuminance / 1000.0;
  }

  void setMaxLuminance(double in) {
    maxLuminance = in * 1000.0;
    emit syncParamsChanged();
  }

  double getChromaBoost() {
    return chromaBoost / 1000.0;
  }

  void setChromaBoost(double in) {
    chromaBoost = in * 1000.0;
    emit syncParamsChanged();
  }

  double getLumaBoost() {
    return lumaBoost / 1000.0;
  }

  void setLumaBoost(double in) {
    lumaBoost = in * 1000.0;
    emit syncParamsChanged();
  }

  double getColorBias() {
    return colorBias;
  }

  void setColorBias(double in) {
    colorBias = in;
    emit syncParamsChanged();
  }

  double getCenterSlowness() {
    return centerSlowness / 1000.0;
  }

  void setCenterSlowness(double in) {
    centerSlowness = in * 1000.0;
    emit syncParamsChanged();
  }

  double getSideSlowness() {
    return sideSlowness / 1000.0;
  }

  void setSideSlowness(double in) {
    sideSlowness = in * 1000.0;
    emit syncParamsChanged();
  }

  double getSatThreshold() {
    return satThreshold / 1000.0;
  }

  void setSatThreshold(double in) {
    satThreshold = in * 1000.0;
    emit syncParamsChanged();
  }

  double getValThreshold() {
    return valThreshold / 1000.0;
  }

  void setValThreshold(double in) {
    valThreshold = in * 1000.0;
    emit syncParamsChanged();
  }

  QImage getImage(int width);

public slots:
  void connectBridges();

signals:
  void hueInit();

private:
  BridgeDiscovery* bridgeDiscovery;
  QList<EntertainmentGroup*> entertainmentGroups;

  //-----------------------------------------------
  // SCREEN SYNC ----------------------------------
public:
  Q_INVOKABLE void detectBridges();
  Q_INVOKABLE bool startCapturing();
  Q_INVOKABLE bool startCapturing(QString capdevice);
  Q_INVOKABLE bool stopCapturing();
  Q_INVOKABLE void startScreenSync();
  Q_INVOKABLE void startScreenSync(QString capdevice, QString syncgroup);
  Q_INVOKABLE void startScreenSync(int eGroupIdx);
  Q_INVOKABLE void startScreenSync(EntertainmentGroup* eGroup);
  Q_INVOKABLE void stopScreenSync();
  Q_INVOKABLE void refreshGroups();
  Q_INVOKABLE QString getError();

  int getLastFrameReadElapsed() const;
  quint64 getTotalFrameReadElapsed() const;
  quint64 getFrameReads() const;
  QSize getLastFrameSize();

signals:
  void entertainmentGroupsChanged();
  void syncingChanged();
  void capturingChanged();
  void triggerStopCapturing();
  void triggerPauseCapturing();
  void triggerResumeCapturing();
  void triggerChangeInterval(int msec);

  void frameReadElapsedChanged();
  void messageSendElapsedChanged();

  void captureParamsChanged();
  void syncParamsChanged();

public slots:
  void updateEntertainmentGroups();
  void onNewImageAvailable(QImage image);

private:
  bool m_syncing = false;
  QString m_syncingGroup;
  QString m_captureDevice;
  QString m_captureVideoStandard;
  int m_captureMultiplier;
  QRect m_captureMargins;
  bool m_capturing = false;
  QString m_lastError;

  QThread* m_captureThread = nullptr;
  Capture* m_framegrabber = nullptr;

  void runSync(EntertainmentGroup* eGroup);
  QMutex eThreadMutex;
  friend class ScreenSyncImageProvider;
  class ScreenSyncImageProvider* eImageProvider;

  QAtomicInteger<int> m_lastFrameWidth;
  QAtomicInteger<int> m_lastFrameHeight;
  QAtomicInteger<quint64> m_lastFrameReadElapsed;
  QAtomicInteger<quint64> m_totalFrameReadElapsed;
  QAtomicInteger<quint64> m_frameReads;
  int captureInterval;
  class EntertainmentGroup* streamingGroup;

  QReadWriteLock screenLock;
  QImage screenSyncScreen;

  QAtomicInteger<qint64> minLuminance;
  QAtomicInteger<qint64> maxLuminance;
  QAtomicInteger<qint64> chromaBoost;
  QAtomicInteger<qint64> lumaBoost;
  QAtomicInteger<qint32> colorBias;
  QAtomicInteger<qint64> centerSlowness;
  QAtomicInteger<qint64> sideSlowness;
  QAtomicInteger<qint64> satThreshold;
  QAtomicInteger<qint64> valThreshold;

private slots:
  void isStreamingChanged(bool isStreaming);
  void streamingGroupDestroyed();
  // END SCREEN SYNC ------------------------------
  //-----------------------------------------------
};
