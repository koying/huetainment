#ifndef CAPTURE_H
#define CAPTURE_H

#include <QVideoFrame>
#include <QThread>
#include <QTimer>

#include <chrono>

class ICaptureImplementation;

class Capture : public QObject
{
  Q_OBJECT
public:
  explicit Capture(QObject *parent = nullptr);
  ~Capture();

  bool Init();
  void Start();
  void Stop();
  bool Cleanup();

  QString GetError() { return m_error; }

signals:
  void NewImageAvailable(QImage image);
  void triggerTimerPause();
  void triggerTimerResume();
  void triggerTimerRestart(int msec);

public:
  QString getDevice() { return m_capDevice; }

public slots:
  void setFrameChangeInterval(int interval);
  void setDevice(const QString& device);
  void setVideoStandard(const QString& videoStandard);
  void Pause();
  void Resume();

private slots:
  void onFrameTimerTick();

private:
  ICaptureImplementation* m_implem;
  QTimer m_frameTimer;
  QString m_capDevice;
  QString m_capSource;
  QString m_capDetail;
  QString m_videoStandard;
  int m_frameInterval;
  QString m_error;

  void onNewFrameAvailable(QVideoFrame frame);
};

#endif // CAPTURE_H
