#include "capture.h"

#include "utils/rgbyuv.h"
#include "libyuv.h"

#include <QDebug>
#include <QElapsedTimer>

//#define DEBUG_VERBOSE

#ifdef ENABLE_DEMO_SOURCE
//#include "capture_sources/CaptureDemo.h"
#include "capture_sources/CaptureDemoYUV.h"
#endif
#ifdef ENABLE_V4L2_SOURCE
#include "capture_sources/CaptureV4L2.h"
#endif
#ifdef ENABLE_SCREEN_SOURCE
#include "capture_sources/CaptureScreenshot.h"
#endif
#ifdef ENABLE_SHAREDMEM_SOURCE
#include "capture_sources/CaptureSharedmem.h"
#endif
#ifdef ENABLE_UDP_SOURCE
#include "capture_sources/CaptureUDP.h"
#endif

#include <iostream>

Capture::Capture(QObject *parent)
  : QObject(parent)
  , m_implem(nullptr)
  , m_frameTimer(this)
  , m_frameInterval(100)
{
}

Capture::~Capture()
{
  Cleanup();
}

bool Capture::Init()
{ 
  if (false);
#ifdef ENABLE_DEMO_SOURCE
  else if (m_capSource == "demo")
    m_implem = new CaptureDemoYUV(this);
  //m_implem = new CaptureDemo(this);
#endif
#ifdef ENABLE_V4L2_SOURCE
  else if (m_capSource == "v4l2")
    m_implem = new CaptureV4L2(this);
#endif
#ifdef ENABLE_SCREEN_SOURCE
  else if (m_capSource == "screen")
    m_implem = new CaptureScreenshot(this);
#endif
#ifdef ENABLE_SHAREDMEM_SOURCE
  else if (m_capSource == "sharedmem")
    m_implem = new CaptureSharedmem(this);
#endif
#ifdef ENABLE_UDP_SOURCE
  else if (m_capSource == "udp")
    m_implem = new CaptureUDP(this);
#endif
  else
  {
    m_error = "Unknown source: " + m_capSource;
    std::cout << "Capture error: " << m_error.toStdString() << std::endl;
    return false;
  }

  m_implem->setDevice(m_capDetail);
  m_implem->setVideoStandard(m_videoStandard);

  if (m_implem->Open() != ICaptureImplementation::RetCode::OK)
  {
    m_error = m_implem->errorMessage();
    std::cout << "Capture error: " << m_error.toStdString() << std::endl;
    return false;
  }

  m_frameTimer.setInterval(m_frameInterval);
  connect(&m_frameTimer, SIGNAL(timeout()), this, SLOT(onFrameTimerTick()));
  connect(this, SIGNAL(triggerTimerPause()), &m_frameTimer, SLOT(stop()));
  connect(this, SIGNAL(triggerTimerResume()), &m_frameTimer, SLOT(start()));
  connect(this, SIGNAL(triggerTimerRestart(int)), &m_frameTimer, SLOT(start(int)));
  m_frameTimer.start();

  return true;
}

bool Capture::Cleanup()
{
  m_implem->Close();
  delete m_implem;
  return true;
}

void Capture::Pause()
{
  emit triggerTimerPause();
}

void Capture::Resume()
{
  emit triggerTimerResume();
}

void Capture::setFrameChangeInterval(int interval)
{
  m_frameInterval = interval;
  triggerTimerRestart(m_frameInterval);
}

void Capture::setDevice(const QString& device)
{
  m_capDevice = device;

  QStringList l = m_capDevice.split(':');
  m_capSource = l[0];
  m_capDetail = l[1];
}

void Capture::setVideoStandard(const QString& videoStandard)
{
  m_videoStandard = videoStandard;
}

void Capture::onFrameTimerTick()
{
#ifdef DEBUG_VERBOSE
  static QElapsedTimer timertick;
  qDebug() << ">> onFrameTimerTick";
#endif
  ICaptureImplementation::RetCode ret = m_implem->RequestNewFrame();
  if (ret == ICaptureImplementation::RetCode::OK)
  {
    QVideoFrame frame = m_implem->GetFrame();
    onNewFrameAvailable(frame);
  }
  else if (ret == ICaptureImplementation::RetCode::ERROR)
  {
    m_error = m_implem->errorMessage();
    std::cout << "Capture error: " << m_error.toStdString() << std::endl;
  }
  else if (ret == ICaptureImplementation::RetCode::TRY_AGAIN)
  {
    // Just skip
  }
#ifdef DEBUG_VERBOSE
  qDebug() << "onFrameTimerTick: " << timertick.restart();
#endif
}

void Capture::onNewFrameAvailable(QVideoFrame frame)
{
  QImage image;
#ifdef DEBUG_VERBOSE
  static QElapsedTimer timernewframe;

  QElapsedTimer timer;
  timer.start();
  qDebug() << ">> onNewFrameAvailable";
#endif

  frame.map(QAbstractVideoBuffer::ReadOnly);
  QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(frame.pixelFormat());
  if (imageFormat != QImage::Format_Invalid)
  {
    QImage tmp_img = QImage(frame.bits(),
                              frame.width(),
                              frame.height(),
                              imageFormat);
    image = tmp_img.copy();
  }
  else
  {

    switch(frame.pixelFormat())
    {
      case QVideoFrame::Format_YUV420P:
      {
        image = QImage(frame.width(), frame.height(), QImage::Format_RGBA8888);
        convert_yuv420_to_rgb24((const unsigned char*)frame.bits(), (unsigned char*)image.scanLine(0), frame.width(), frame.height(), 0);
      }
        break;
      case QVideoFrame::Format_YUYV:
      {
        image = QImage(frame.width(), frame.height(), QImage::Format_ARGB32);
        libyuv::YUY2ToARGB((const uint8_t*)frame.bits(), frame.bytesPerLine(), (unsigned char*)image.scanLine(0), image.bytesPerLine(), frame.width(), frame.height());

      }
        break;
    }
  }
  frame.unmap();

#ifdef DEBUG_VERBOSE
  qDebug() << "onNewFrameAvailable: " << timer.elapsed() << "; global: " << timernewframe.restart();
#endif

  emit NewImageAvailable(image);
}
