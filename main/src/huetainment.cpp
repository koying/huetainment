#include "huetainment.h"
#include "entertainment.h"
#include "utility.h"

#include <algorithm>
#include <cmath>

#include "huebridge.h"
#include "objectmodel.h"
#include "bridgediscovery.h"

#include <QColor>

#include <QElapsedTimer>
#include <QTimer>
#include <QSettings>
#include <QCoreApplication>
#include <QImage>

//#define DEBUG_VERBOSE

QNetworkAccessManager* qnam = nullptr;
static QMutex huetainmentLock;
static Huetainment* huetainmentInstance = nullptr;

Huetainment::Huetainment(QObject *parent)
  : QObject(parent)
{
  if(qnam == nullptr)
    qnam = new QNetworkAccessManager(nullptr);

  detectBridges();

  emit hueInit();

  ReadSettings();

  //ENTERTAINMENT
  streamingGroup = nullptr;

  huetainmentLock.lock();
  huetainmentInstance = this;
  huetainmentLock.unlock();

  QTimer *timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SIGNAL(frameReadElapsedChanged()));
  timer->start(500);
}

Huetainment::~Huetainment()
{
  WriteSettings();

  huetainmentLock.lock();
  if(huetainmentInstance == this)
  {
    huetainmentInstance = nullptr;
  }
  huetainmentLock.unlock();

  for (QObject* Obj : bridgeDiscovery->getModel())
  {
    HueBridge* bridge = qobject_cast<HueBridge*>(Obj);
    if (bridge != nullptr)
    {
      for (auto& group : bridge->EntertainmentGroups)
      {
        group->shutDownImmediately();
        while (group->hasRunningThread())
        {
          QThread::msleep(100);
        }
      }
    }
  }
}

void Huetainment::ReadSettings()
{
  QSettings settings("Semperpax", "huetainment", this);
  settings.beginGroup("entertainment");

  qDebug() << "Settings file: " << settings.fileName();

  m_captureDevice =         settings.value("captureDevice", "").toString();
  m_captureVideoStandard =  settings.value("captureVideoStandard", "").toString();
  m_captureMargins =        settings.value("captureMargins", "").toRect();
  m_captureMultiplier =     settings.value("captureMultiplier", 1).toInt();
  m_syncingGroup =          settings.value("syncingGroup", "").toString();
  setCaptureInterval(     settings.value("captureInterval",	25).toInt());
  setMaxLuminance(	  settings.value("maxLuminance",		1.0).toDouble());
  setMinLuminance(	  settings.value("minLuminance",		0.0).toDouble());
  setChromaBoost(	  settings.value("chromaBoost",		1.0).toDouble());
  setLumaBoost(		  settings.value("lumaBoost",			1.5).toDouble());
  setColorBias(		  settings.value("colorBias",			75).toDouble());
  setCenterSlowness(	  settings.value("centerSlowness",	8.9).toDouble());
  setSideSlowness(	  settings.value("sideSlowness",		10.0).toDouble());
  setSatThreshold(  settings.value("satThreshold",		0.1).toDouble());
  setValThreshold(  settings.value("valThreshold",		0.1).toDouble());
  settings.endGroup();

  emit syncParamsChanged();
  emit captureParamsChanged();
}

void Huetainment::WriteSettings()
{
  QSettings settings("Semperpax", "huetainment", this);
  settings.beginGroup("entertainment");

  qDebug() << "Settings file: " << settings.fileName();

  settings.setValue("captureDevice",              m_captureDevice);
  settings.setValue("captureVideoStandard",       m_captureVideoStandard);
  settings.setValue("captureMargins",             m_captureMargins);
  settings.setValue("captureMultiplier",          m_captureMultiplier);
  settings.setValue("syncingGroup",               m_syncingGroup);
  settings.setValue("captureInterval",	        getCaptureInterval());
  settings.setValue("maxLuminance",		getMaxLuminance());
  settings.setValue("minLuminance",		getMinLuminance());
  settings.setValue("chromaBoost",		getChromaBoost());
  settings.setValue("lumaBoost",			getLumaBoost());
  settings.setValue("colorBias",			getColorBias());
  settings.setValue("centerSlowness",		getCenterSlowness());
  settings.setValue("sideSlowness",		getSideSlowness());
  settings.setValue("satThreshold",		getSatThreshold());
  settings.setValue("valThreshold",		getValThreshold());

  settings.endGroup();
}

void Huetainment::ResetSettings()
{
  QSettings settings("Semperpax", "huetainment", this);
  settings.beginGroup("entertainment");
  settings.remove("");
  settings.endGroup();

  ReadSettings();
}

int Huetainment::getMessageSendElapsed()
{
  return -1;
}

void Huetainment::setCaptureInterval(int interval)
{
  captureInterval = interval;
  emit triggerChangeInterval(interval);
  emit captureParamsChanged();
}

void Huetainment::detectBridges()
{
  bridgeDiscovery = new BridgeDiscovery(this);
  bridgeDiscovery->startSearch();

  connect(bridgeDiscovery, SIGNAL(modelChanged()),
          this, SLOT(connectBridges()));
}

//A color sample in CIE L*C*h?
struct LChSample
{
  double L; //L*, the lightness
  double C; //C*, the chromaticity / "saturation"
  double h; //h?, the hue

  double maxRGB; //highest R,G,B value
};

void Huetainment::startScreenSync()
{
  startScreenSync(m_captureDevice, m_syncingGroup);
}

void Huetainment::startScreenSync(QString capdevice, QString syncgroup)
{
  if (syncgroup.isEmpty())
    syncgroup = m_syncingGroup;
  if (capdevice.isEmpty())
    capdevice = m_captureDevice;

  if (m_framegrabber && m_framegrabber->getDevice() != capdevice)
    stopCapturing();

  if (!m_framegrabber)
    startCapturing(capdevice);

  for (auto eGroup : entertainmentGroups)
  {
    if (eGroup->name == syncgroup)
    {
      HueBridge* bridge = eGroup->bridgeParent();
      bridge->refreshLightsSync();

      startScreenSync(eGroup);
      return;
    }
  }
}

void Huetainment::startScreenSync(int eGroupIdx)
{
  startScreenSync(entertainmentGroups[eGroupIdx]);
}

void Huetainment::startScreenSync(EntertainmentGroup* eGroup)
{
  if(eGroup == nullptr) {
    qWarning() << "null eGroup!!!!!!";
    return;
  }

  m_syncing = true;
  emit syncingChanged();

  runSync(eGroup);
  eGroup->startStreaming([&](const EntertainmentLight& light, double& cx, double& cy, double& cb, double deltaTime)->bool {

    double minBrightness = getMinLuminance();
    double maxBrightness = getMaxLuminance();
    double brightnessBoost = getLumaBoost();
    double chromaBoost = getChromaBoost();

    float satThreshold = getSatThreshold();
    float valThreshold = getValThreshold();

//    qDebug() << "minb:" << minBrightness << "maxb:" << maxBrightness << "bboost:" << brightnessBoost << "cboost: " << chromaBoost;

    screenLock.lockForRead();

#ifdef DEBUG_VERBOSE
  QElapsedTimer timer;
  timer.start();
#endif

    if (screenSyncScreen.width() == 0 || screenSyncScreen.height() == 0)
    {
      screenLock.unlock();
      return false;
    }

    const double width = std::max(1.0 - std::abs(light.x), 0.3);
    const double height = std::max(1.0 - std::abs(light.z), 0.3);

    const int minX = round((std::max(light.x - width, -1.0) + 1.0) * screenSyncScreen.width() / 2.0);
    const int maxX = round((std::min(light.x + width, 1.0) + 1.0) * screenSyncScreen.width() / 2.0);

    const int minY = round((std::max(light.z - height, -1.0) + 1.0) * screenSyncScreen.height() / 2.0);
    const int maxY = round((std::min(light.z + height, 1.0) + 1.0) * screenSyncScreen.height() / 2.0);

#if 1
    double tavgR = 0.0, tavgG = 0.0, tavgB = 0.0;
    double tdomR = 0.0, tdomG = 0.0, tdomB = 0.0;
    int cols = maxX - minX;
    int rows = 0;
    int dom_samples = 0;

    for (int y = minY; y < maxY; ++y)
    {
      double rR = 0.0, rG = 0.0, rB = 0.0;

      for (int x = minX; x < maxX; ++x)
      {
        double pR = (double)qRed(screenSyncScreen.pixel(x, y)) / 255.0;
        double pG = (double)qGreen(screenSyncScreen.pixel(x, y)) / 255.0;
        double pB = (double)qBlue(screenSyncScreen.pixel(x, y)) / 255.0;

        rR += pR; rG += pG; rB += pB;

        double min = std::min(pR, std::min(pG, pB));
        double pV = std::max(pR, std::max(pG, pB));
        double pS = pV == 0 ? 0 : (pV - min) / pV;
        if (pS > satThreshold)
        {
          tdomR += pR;
          tdomG += pG;
          tdomB += pB;
          ++dom_samples;
        }
      }
      rR /= cols; rG /= cols; rB /= cols;

      // ignore black rows
      double min = std::min(rR, std::min(rG, rB));
      double rV = std::max(rR, std::max(rG, rB));
      double rS = rV == 0 ? 0 : (rV - min) / rV;
      if (rV > valThreshold)
      {
        tavgR += rR;
        tavgG += rG;
        tavgB += rB;
        ++rows;
      }
    }

    screenLock.unlock();

    if (rows < (maxY - minY) / 2)
    {
      // If screen is mostly black, just return black;
      cb = 0.0;
      return true;
    }

    tavgR /= rows; tavgG /= rows; tavgB /= rows;
    double use_brightness = (0.2126 *tavgR + 0.7152 *tavgG + 0.0722 *tavgB);
    if (dom_samples > rows * cols / 4) // If more than 25% usable pixels -> chroma
    {
//      qInfo() << "DOM" << dom_samples;
      tavgR = tdomR / dom_samples; tavgG = tdomG / dom_samples; tavgB = tdomB / dom_samples;
    }
    else
    {
//      qInfo() << "AVG" << rows;
    }

#else
    double tR = 0.0, tG = 0.0, tB = 0.0;
    int samples = 0;

    for (int y = minY; y < maxY; ++y)
    {
      for (int x = minX; x < maxX; ++x)
      {
        double pR = (double)qRed(screenSyncScreen.pixel(x, y)) / 255.0;
        double pG = (double)qGreen(screenSyncScreen.pixel(x, y)) / 255.0;
        double pB = (double)qBlue(screenSyncScreen.pixel(x, y)) / 255.0;

        double min = std::min(pR, std::min(pG, pB));
        double max = std::max(pR, std::max(pG, pB));
        double pS = max == 0 ? 0 : (max - min) / max;
        if (pS > (1.0 - chromaBoost))
        {
          tR += pR;
          tG += pG;
          tB += pB;
          ++samples;
        }
      }
    }

    screenLock.unlock();

    if (samples < (maxY - minY) * (maxX - minX) / 10) // If less than 10% usable pixels -> black
      tR = tG = tB = 0.0;
    else
      tR /= samples; tG /= samples; tB /= samples;
#endif
#ifdef DEBUG_VERBOSE
  qDebug() << "screenLock: " << timer.elapsed() << " ms";
#endif

    Color::rgb2xyB(tavgR, tavgG, tavgB, cx, cy, cb);

    // Skip imperceptible color updates (+ bias)
    double u, v;
    Color::xy2uv(cx, cy, u, v);
    double old_u, old_v;
    Color::xy2uv(light.x, light.y, old_u, old_v);

    double biasC = float(((100 - getColorBias()) / 5)+1) * 0.0011f;

    double color_dist = sqrt(pow(u - old_u, 2) + pow(v - old_v, 2));
    if (color_dist < biasC)
    {
      cx = light.cx;
      cy = light.cy;
      cb = light.cb;
    }
    else
    {
      double min = std::min(tavgR, std::min(tavgG, tavgB));
      double V = std::max(tavgR, std::max(tavgG, tavgB));
      double S = V == 0 ? 0 : (V - min) / V;
      if (V < valThreshold) // ignore brigtness adjustments in low saturation
      {
        cb = std::min(cb, use_brightness);
      }
      else
      {
        double brightness = std::min(1.0, use_brightness * brightnessBoost);
        cb = brightness * (maxBrightness - minBrightness) + minBrightness;
      }
    }
//    qInfo() << "Plain mean xyB:" << cx << cy << cb;

    return true;
  });
}
void Huetainment::stopScreenSync()
{
  m_syncing = false;
  emit syncingChanged();

  //TODO: this seems like a flaw...
  for (QObject* Obj : bridgeDiscovery->getModel())
  {
    HueBridge* bridge = qobject_cast<HueBridge*>(Obj);
    if (bridge != nullptr)
    {
      for (auto& group : bridge->EntertainmentGroups)
      {
        if (group->isStreaming)
        {
          group->stopStreaming();
        }
      }
    }
  }
  if (m_captureThread)
    stopCapturing();
}

void Huetainment::refreshGroups()
{
  for (QObject* Obj : bridgeDiscovery->getModel())
  {
    HueBridge* bridge = qobject_cast<HueBridge*>(Obj);
    if (bridge != nullptr)
    {
      for (auto& group : bridge->EntertainmentGroups)
      {
        if (group->isStreaming)
        {
          //refuse to modify the groups list if one is syncing right now (causes havoc at the moment)
          return;
        }
      }
    }
  }

  for (QObject* Obj : bridgeDiscovery->getModel())
  {
    HueBridge* bridge = qobject_cast<HueBridge*>(Obj);
    if (bridge != nullptr)
    {
      bridge->requestGroups();
    }
  }
}

QString Huetainment::getError()
{
  return m_lastError;
}

void Huetainment::updateEntertainmentGroups()
{
  entertainmentGroups.clear();

  for (QObject* Obj : bridgeDiscovery->getModel())
  {
    HueBridge* bridge = qobject_cast<HueBridge*>(Obj);
    if (bridge != nullptr)
    {
      for (auto& group : bridge->EntertainmentGroups)
      {
        entertainmentGroups.push_back(group);
      }
    }
  }

  emit entertainmentGroupsChanged();
}

void Huetainment::connectBridges()
{
  for (QObject* Obj : bridgeDiscovery->getModel())
  {
    HueBridge* bridge = qobject_cast<HueBridge*>(Obj);
    if (bridge != nullptr)
    {
      connect(bridge, SIGNAL(entertainmentGroupsChanged()),
              this, SLOT(updateEntertainmentGroups()));
      connect(bridge, SIGNAL(lightsChanged()),
              this, SIGNAL(entertainmentGroupsChanged()));
    }
  }
}

bool Huetainment::startCapturing()
{
  return startCapturing(m_captureDevice);
}

bool Huetainment::startCapturing(QString capdevice)
{
  if (capdevice.isEmpty())
    capdevice = m_captureDevice;

  m_totalFrameReadElapsed = 0;
  m_frameReads = 0;

  if (m_captureThread)
  {
    m_captureThread->quit();
    m_capturing = false;
  }

  m_captureThread = new QThread(this);
  m_captureThread->setObjectName("Capture thread");
  m_framegrabber = new Capture();
  m_framegrabber->setDevice(capdevice);
  m_framegrabber->setVideoStandard(m_captureVideoStandard);
  if (m_framegrabber->Init())
  {
    m_framegrabber->moveToThread(m_captureThread);
    connect(m_framegrabber, SIGNAL(NewImageAvailable(QImage)), this, SLOT(onNewImageAvailable(QImage)));
    connect(m_captureThread, SIGNAL(finished()), m_framegrabber, SLOT(deleteLater()));
    connect(this, SIGNAL(triggerStopCapturing()), m_captureThread, SLOT(quit()));
    connect(this, SIGNAL(triggerChangeInterval(int)), m_framegrabber, SLOT(setFrameChangeInterval(int)));
    connect(this, SIGNAL(triggerPauseCapturing()), m_framegrabber, SLOT(Pause()));
    connect(this, SIGNAL(triggerResumeCapturing()), m_framegrabber, SLOT(Resume()));

    m_framegrabber->setFrameChangeInterval(captureInterval);

    m_captureThread->start();
    m_capturing = true;
  }
  else
  {
    m_lastError = m_framegrabber->GetError();
    m_framegrabber->deleteLater();
    m_framegrabber = nullptr;
    return false;
  }

  return true;
}

bool Huetainment::stopCapturing()
{
  m_capturing = false;
  disconnect(m_framegrabber, SIGNAL(NewImageAvailable(QImage)));
  emit triggerStopCapturing();
  m_captureThread = nullptr;
  m_framegrabber = nullptr;
  return true;
}

void Huetainment::onNewImageAvailable(QImage image)
{
  static QElapsedTimer timer;

  if (image.isNull())
    return;

  screenLock.lockForWrite();

#ifdef DEBUG_VERBOSE
  QElapsedTimer timerlock;
  timerlock.start();
  qDebug() << ">> onNewImageAvailable";
#endif

  const int WidthBuckets = 16 * m_captureMultiplier;
  const int HeightBuckets = 9 * m_captureMultiplier;

  m_lastFrameWidth = image.width();
  m_lastFrameHeight = image.height();
  if (!m_captureMargins.isNull())
  {
    QMargins margins = QMargins(m_captureMargins.x(), m_captureMargins.y(), m_captureMargins.width(), m_captureMargins.height());
    QRect destR = image.rect().marginsRemoved(margins);
    image = image.copy(destR);
  }
  screenSyncScreen = image.scaled(QSize(WidthBuckets, HeightBuckets), Qt::IgnoreAspectRatio, Qt::FastTransformation);
#ifdef DEBUG_VERBOSE
  qDebug() << "onNewImageAvailable: " << timerlock.elapsed() << " ms";
#endif

  screenLock.unlock();

  m_lastFrameReadElapsed = timer.restart();
  if (m_lastFrameReadElapsed < 10000)
  {
    m_totalFrameReadElapsed += m_lastFrameReadElapsed;
    m_frameReads++;
  }
  //emit frameReadElapsedChanged();
}


void Huetainment::runSync(EntertainmentGroup* eGroup)
{
  HueBridge* bridge = eGroup->bridgeParent();
  Q_ASSERT(bridge != nullptr);

  if (bridge->username.isEmpty() || bridge->clientkey.isEmpty())
  {
    qWarning() << "Can't start entertainment thread unless we're logged into the bridge";
    return;
  }

  if (streamingGroup != nullptr && streamingGroup != eGroup)
  {
    disconnect(streamingGroup, &EntertainmentGroup::isStreamingChanged, this, &Huetainment::isStreamingChanged);
    disconnect(streamingGroup, &EntertainmentGroup::destroyed, this, &Huetainment::streamingGroupDestroyed);

    streamingGroup->stopStreaming();
    streamingGroup = nullptr;
  }

  streamingGroup = eGroup;
  connect(streamingGroup, &EntertainmentGroup::isStreamingChanged, this, &Huetainment::isStreamingChanged);
  connect(streamingGroup, &EntertainmentGroup::destroyed, this, &Huetainment::streamingGroupDestroyed);
}

quint64 Huetainment::getFrameReads() const
{
  return m_frameReads;
}

quint64 Huetainment::getTotalFrameReadElapsed() const
{
  return m_totalFrameReadElapsed;
}

int Huetainment::getLastFrameReadElapsed() const
{
  return m_lastFrameReadElapsed;
}

QSize Huetainment::getLastFrameSize()
{
  return QSize(m_lastFrameWidth, m_lastFrameHeight);
}

void Huetainment::isStreamingChanged(bool isStreaming)
{
  if (isStreaming)
    emit triggerResumeCapturing();
  else
    emit triggerPauseCapturing();
}

void Huetainment::streamingGroupDestroyed()
{
  streamingGroup = nullptr;
}

QImage Huetainment::getImage(int width)
{
  QImage img = screenSyncScreen.scaled(QSize(width, width), Qt::KeepAspectRatio, Qt::FastTransformation);
  return img;
}

