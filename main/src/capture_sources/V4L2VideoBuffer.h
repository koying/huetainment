#pragma once

#include <QAbstractVideoBuffer>

#include <linux/videodev2.h>

class V4L2VideoBuffer: public QAbstractVideoBuffer
{
public:
  V4L2VideoBuffer();

  bool allocate(int fd, int index, int rowstride);
  bool free();
  QString getError();

  QVariant handle() const override;
  QAbstractVideoBuffer::HandleType handleType() const;
  QAbstractVideoBuffer::MapMode mapMode() const override;
  uchar *map(QAbstractVideoBuffer::MapMode mode, int *numBytes, int *bytesPerLine) override;
  void unmap() override;
  void release() override;

private:
  QString m_error;
  int m_fd;
  size_t m_index;
  uint32_t m_rowstride;
  void *m_data;
  size_t m_size;
  struct v4l2_buffer m_v4l2buf;

  bool error(QString msg);
};
