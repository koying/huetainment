#ifndef ICAPTUREIMPLEMENTATION_H
#define ICAPTUREIMPLEMENTATION_H

#include <QString>
#include <QVideoFrame>

class ICaptureImplementation
{
public:
  enum RetCode
  {
    OK,
    TRY_AGAIN,
    ERROR
  };

public:
  virtual ~ICaptureImplementation() {}
  virtual ICaptureImplementation::RetCode Open() = 0;
  virtual ICaptureImplementation::RetCode RequestNewFrame() = 0;
  virtual QVideoFrame GetFrame() = 0;
  virtual ICaptureImplementation::RetCode Close() = 0;

  QString errorMessage() const
  {
    return m_errorMessage;
  }

  virtual QString device() const;
  virtual void setDevice(const QString& device);

  virtual QString videoStandard() const;
  virtual void setVideoStandard(const QString& videoStandard);

protected:
  QString m_errorMessage;
  QString m_device;
  QString m_videoStandard;

  ICaptureImplementation::RetCode Error(QString error)
  {
    m_errorMessage = error;
    return ICaptureImplementation::RetCode::ERROR;
  }
};

#endif // ICAPTUREIMPLEMENTATION_H
