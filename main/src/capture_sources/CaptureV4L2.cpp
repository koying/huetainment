#include "CaptureV4L2.h"

#include <QDebug>
#include <QElapsedTimer>
//#define DEBUG_VERBOSE

#include <assert.h>
#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <string.h> // strerrno
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

#include "V4L2VideoBuffer.h"

#define V4L2_DEFAULT_DEVICE "/dev/video0"

#define CLEAR(x) memset(&(x), 0, sizeof(x))

static int xioctl(int fh, unsigned long int request, void *arg)
{
  int r;

  do
  {
    r = ioctl(fh, request, arg);
  } while (-1 == r && EINTR == errno);

  return r;
}

/* */

CaptureV4L2::CaptureV4L2(QObject *parent)
  : QObject(parent)
  , m_fd(-1)
  , m_numBuffers(0)
  , m_buffers(nullptr)
{
  m_device = V4L2_DEFAULT_DEVICE;
}

CaptureV4L2::~CaptureV4L2()
{
  if (m_fd != -1)
    Close();
}

ICaptureImplementation::RetCode CaptureV4L2::SetupCapture()
{
  struct v4l2_capability cap;
  struct v4l2_cropcap cropcap;
  struct v4l2_crop crop;
  struct v4l2_format fmt;

  if (-1 == xioctl(m_fd, VIDIOC_QUERYCAP, &cap))
  {
    if (EINVAL == errno)
      return Error(m_device + " is no V4L2 device");
    else
      return Error("VIDIOC_QUERYCAP");
  }

  if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
    return Error(m_device + " is no video capture device");

  if (!(cap.capabilities & V4L2_CAP_STREAMING))
    return Error(m_device + " does not support streaming i/o");

  /* Select video input, video standard and tune here. */

  CLEAR(cropcap);

  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (0 == xioctl(m_fd, VIDIOC_CROPCAP, &cropcap))
  {
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    crop.c = cropcap.defrect; /* reset to default */

    if (-1 == xioctl(m_fd, VIDIOC_S_CROP, &crop))
    {
      switch (errno) {
        case EINVAL:
          qInfo() << "V4L2: Cropping not supported.";
          break;
        default:
          /* Errors ignored. */
          break;
      }
    }
  } else {
    /* Errors ignored. */
  }

  struct v4l2_input input;

  CLEAR(input);

  if (-1 == xioctl(m_fd, VIDIOC_G_INPUT, &input.index))
    return Error("VIDIOC_G_INPUT");

  if (-1 == xioctl(m_fd, VIDIOC_ENUMINPUT, &input))
    return Error("VIDIOC_ENUMINPUT");

  bool supports_video_standards = true;
  qInfo() << QString::asprintf("Current input \"%s\" supported video standards:", input.name);

  v4l2_std_id std_id;
  if (-1 == xioctl(m_fd, VIDIOC_G_STD, &std_id))
  {
    /* Note when VIDIOC_ENUMSTD always returns ENOTTY this
         is no video device or it falls under the USB exception,
         and VIDIOC_G_STD returning ENOTTY is no error. */

    supports_video_standards = false;
    qInfo() << "None";
  }

  if (supports_video_standards)
  {
    v4l2_std_id tobe_std_id = std_id;

    struct v4l2_standard standard;
    CLEAR(standard);
    standard.index = 0;

    while (0 == xioctl(m_fd, VIDIOC_ENUMSTD, &standard))
    {
      QString std_name((const char *)standard.name);
      if (standard.id & input.std)
      {
        if (std_id & standard.id)
          qInfo() << std_name << " (current)";
        else
          qInfo() << std_name;
      }
      if (std_name == m_videoStandard)
        tobe_std_id = standard.id;

      standard.index++;
    }

    /* EINVAL indicates the end of the enumeration, which cannot be
     empty unless this device falls under the USB exception. */

    if (errno != EINVAL || standard.index == 0)
      return Error("VIDIOC_ENUMINPUT empty");

    if (-1 == xioctl(m_fd, VIDIOC_S_STD, &tobe_std_id))
      return Error("VIDIOC_S_STD");
  }

  CLEAR(fmt);

  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (false /*force_format*/)
  {
    fmt.fmt.pix.width       = 1280;
    fmt.fmt.pix.height      = 720;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_BGR32;
    fmt.fmt.pix.field       = V4L2_FIELD_NONE;

    if (-1 == xioctl(m_fd, VIDIOC_S_FMT, &fmt))
      return Error("VIDIOC_S_FMT");

    if (fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_ARGB32)
      // note that libv4l2 (look for 'v4l-utils') provides helpers
      // to manage conversions
      return Error("V4L2 device does not support ARGB32 format. Support for more format need to be added!");
  }
  else
  {
    /* Preserve original settings as set by v4l2-ctl for example */
    if (-1 == xioctl(m_fd, VIDIOC_G_FMT, &fmt))
      return Error("VIDIOC_G_FMT");
  }

  m_xres = fmt.fmt.pix.width;
  m_yres = fmt.fmt.pix.height;
  m_rowstride = fmt.fmt.pix.bytesperline;
  m_pixelstride = fmt.fmt.pix.bytesperline / fmt.fmt.pix.width;

  switch (fmt.fmt.pix.pixelformat)
  {
    case V4L2_PIX_FMT_BGR32:
      m_format = QVideoFrame::Format_BGR32;
      break;
    case V4L2_PIX_FMT_ABGR32:
      m_format = QVideoFrame::Format_BGRA32;
      break;
    case V4L2_PIX_FMT_RGB32:
      m_format = QVideoFrame::Format_RGB32;
      break;
    case V4L2_PIX_FMT_ARGB32:
      m_format = QVideoFrame::Format_ARGB32;
      break;
    case V4L2_PIX_FMT_RGB24:
      m_format = QVideoFrame::Format_RGB24;
      break;
    case V4L2_PIX_FMT_YUV420:
      m_format = QVideoFrame::Format_YUV420P;
      break;
    case V4L2_PIX_FMT_YUYV:
      m_format = QVideoFrame::Format_YUYV;
      break;
    default:
      return Error(QString::asprintf("Input format not supported: %x", fmt.fmt.pix.pixelformat));
  }
  qInfo() << "Setup V4L2 capture at" << QString::asprintf("%dx%d", m_xres, m_yres);

  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureV4L2::Open()
{
  auto ret = ICaptureImplementation::OK;
  struct stat st;

  if (-1 == stat(m_device.toStdString().c_str(), &st))
    return Error(m_device + ": cannot identify! " + QString(errno) +  ": " + strerror(errno));

  if (!S_ISCHR(st.st_mode))
    return Error(m_device + " is no device");

  m_fd = open(m_device.toStdString().c_str(), O_RDWR /* required */ | O_NONBLOCK, 0);

  if (-1 == m_fd)
    return Error(m_device + ": cannot open! " + QString(errno) + ": " + strerror(errno));

  if (SetupCapture() != ICaptureImplementation::RetCode::OK)
    return ICaptureImplementation::ERROR;

  struct v4l2_requestbuffers req;

  CLEAR(req);

  req.count = 4;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_MMAP;

  if (-1 == xioctl(m_fd, VIDIOC_REQBUFS, &req))
  {
    if (EINVAL == errno)
      return Error(m_device + " does not support memory mapping");
    else
      return Error("V4L2: Buffer request failed");
  }

  if (req.count < 2)
    return Error(QString("Insufficient buffer memory on ") + m_device);

  m_buffers = static_cast<V4L2VideoBuffer**>(calloc(req.count, sizeof(V4L2VideoBuffer*)));
  if (!m_buffers)
    return Error("Out of memory");

  for (m_numBuffers = 0; m_numBuffers < req.count; ++m_numBuffers)
  {
    m_buffers[m_numBuffers] = new V4L2VideoBuffer();
    if (!m_buffers[m_numBuffers]->allocate(m_fd, m_numBuffers, m_rowstride))
      return Error(m_buffers[m_numBuffers]->getError());
  }

  return StartStreaming();
}


ICaptureImplementation::RetCode CaptureV4L2::StartStreaming()
{
  qInfo() << "Start V4L2 capture";
  enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == xioctl(m_fd, VIDIOC_STREAMON, &type))
    return Error("VIDIOC_STREAMON");

  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureV4L2::StopStreaming()
{
  qInfo() << "Stop V4L2 capture";
  enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == xioctl(m_fd, VIDIOC_STREAMOFF, &type))
    return Error("VIDIOC_STREAMOFF");

  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureV4L2::Close()
{
  if (m_fd != -1)
  {
    if (StopStreaming() != ICaptureImplementation::RetCode::OK)
      return ICaptureImplementation::RetCode::ERROR;

    /* */

    m_frame = QVideoFrame(); // Prevents SEGFAULT if buffers are freed
    for (unsigned int i = 0; i < m_numBuffers; ++i)
    {
      if (!m_buffers[i]->free())
        return Error(m_buffers[i]->getError());
      delete m_buffers[i];
    }

    free(m_buffers);

    /* */

    if (-1 == close(m_fd))
      return Error("close");

    m_fd = -1;
  }
  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureV4L2::RequestNewFrame()
{
#ifdef DEBUG_VERBOSE
  QElapsedTimer timer;
  timer.start();
#endif

  struct v4l2_buffer buf;
  CLEAR(buf);

  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;

  if (-1 == xioctl(m_fd, VIDIOC_DQBUF, &buf))
  {
    switch (errno)
    {
      case EAGAIN:
        return ICaptureImplementation::RetCode::TRY_AGAIN;

      case EIO:
        /* Could ignore EIO, see spec. */

        /* fall through */

      default:
        return Error("VIDIOC_DQBUF");
    }
  }

  assert(buf.index < m_numBuffers);
  m_frame = QVideoFrame(m_buffers[buf.index], QSize(m_xres, m_yres), m_format);

#ifdef DEBUG_VERBOSE
  qDebug() << "requestframe: " << timer.elapsed() << " ms";
#endif

  return ICaptureImplementation::RetCode::OK;
}

QVideoFrame CaptureV4L2::GetFrame()
{
  return m_frame;
}

void CaptureV4L2::setDevice(const QString& device)
{
  m_device = device;
  if (m_device.isEmpty())
    m_device = V4L2_DEFAULT_DEVICE;
}
