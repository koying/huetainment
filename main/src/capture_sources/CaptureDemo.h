#ifndef CAPTUREDEMO_H
#define CAPTUREDEMO_H

#include <QObject>
#include <QImage>

#include "ICaptureImplementation.h"

class CaptureDemo : public QObject, public ICaptureImplementation
{
  Q_OBJECT

public:
  explicit CaptureDemo(QObject *parent = nullptr);
  ~CaptureDemo();

  // ICaptureImplementation interface
public:
  ICaptureImplementation::RetCode Open() override;
  ICaptureImplementation::RetCode RequestNewFrame() override;
  ICaptureImplementation::RetCode Close() override;
  QVideoFrame GetFrame() override;

private:
  QImage m_demoImg;
};

#endif // CAPTUREDEMO_H