#include "CaptureUDP.h"

#include <QDebug>
#include <QNetworkDatagram>

#define UDP_PORT 3002

CaptureUDP::CaptureUDP(QObject *parent)
  : QObject(parent)
  , m_buffer(256000, 0)
{
  qInfo() << "Opening UDP source";
}

CaptureUDP::~CaptureUDP()
{
  if (m_opened)
    Close();
}

ICaptureImplementation::RetCode CaptureUDP::Open()
{
  m_serverSocket = new QUdpSocket(this);
  m_serverSocket->bind(UDP_PORT);
  connect(m_serverSocket, SIGNAL(readyRead()), this, SLOT(readDatagrams()));

  m_opened = true;
  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureUDP::RequestNewFrame()
{
  if (m_transactionOffset)
    return ICaptureImplementation::RetCode::OK;
  else
  {
    return ICaptureImplementation::RetCode::TRY_AGAIN;
  }
}

ICaptureImplementation::RetCode CaptureUDP::Close()
{
  m_opened = false;
  return ICaptureImplementation::RetCode::OK;
}

QVideoFrame CaptureUDP::GetFrame()
{
  QDataStream in(m_buffer);
  in.startTransaction();
  in >> m_Img;

  if (!in.commitTransaction())
    return QVideoFrame();

  if (m_Img.isNull())
    qCritical() << "UDP: Null Image";

  m_transactionOffset = 0;
  return QVideoFrame(m_Img);
}

void CaptureUDP::readDatagrams()
{
  while (m_serverSocket->hasPendingDatagrams())
  {
    if (m_serverSocket->pendingDatagramSize() > m_buffer.size() - m_transactionOffset)
    {
      qCritical() << "UDP buffer too small:" << m_serverSocket->pendingDatagramSize();
      m_serverSocket->readDatagram(m_buffer.data(), 0);
      return;
    }
    qint64 received = m_serverSocket->readDatagram(m_buffer.data() + m_transactionOffset, m_buffer.size() - m_transactionOffset);
    m_transactionOffset += received;
//    qDebug() << "UDP: received" << received;
  }
}
