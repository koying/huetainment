#pragma once

#include <QObject>
#include <QImage>
#include <QUdpSocket>
#include <QBuffer>

#include "ICaptureImplementation.h"

class CaptureUDP : public QObject, public ICaptureImplementation
{
  Q_OBJECT

public:
  explicit CaptureUDP(QObject *parent = nullptr);
  ~CaptureUDP();

  // ICaptureImplementation interface
public:
  ICaptureImplementation::RetCode Open() override;
  ICaptureImplementation::RetCode RequestNewFrame() override;
  ICaptureImplementation::RetCode Close() override;
  QVideoFrame GetFrame() override;

private:
  bool m_opened = false;
  int m_transactionOffset = 0;
  QByteArray m_buffer;
  QUdpSocket* m_serverSocket = nullptr;
  QImage m_Img;

private slots:
  void readDatagrams();
};

