#include "CaptureSharedmem.h"

#include <QDebug>

#define SHARED_KEY "huetainmentSharedImage"

CaptureSharedmem::CaptureSharedmem(QObject *parent)
  : QObject(parent)
{
  qInfo() << "Opening shared memory source";
}

CaptureSharedmem::~CaptureSharedmem()
{
  if (m_opened)
    Close();
}

ICaptureImplementation::RetCode CaptureSharedmem::Open()
{
  m_shared = new QSharedMemory(SHARED_KEY, this);
  if (!m_shared)
    return Error("Cannot instanciate shared memory");

  if (!m_shared->create(3840*2160*3))
  {
    switch ((int)m_shared->error())
    {
      case QSharedMemory::AlreadyExists: // ok
        if (!m_shared->attach())
          return Error("Cannot attach to shared memory: " + m_shared->errorString());
        break;
      default:
        return Error("Cannot create shared memory");
    }
  }

  if (!m_shared->lock())
    return Error("Cannot lock shared memory: "+ m_shared->errorString());

  m_Img = QImage(320, 180, QImage::Format_ARGB32);
  m_Img.fill(Qt::red);

  QBuffer buf;
  buf.open(QBuffer::ReadWrite);
  QDataStream out(&buf);
  out << m_Img;

  char *to = (char*)m_shared->data();
  const char *from = buf.data().data();
  memcpy(to, from, qMin(m_shared->size(), (int)buf.size()));
  buf.close();

  m_shared->unlock();

  m_opened = true;
  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureSharedmem::RequestNewFrame()
{
  ICaptureImplementation::RetCode ret = ICaptureImplementation::RetCode::OK;

  if (!m_shared->lock())
    return Error("Cannot lock shared memory: "+ m_shared->errorString());

  m_buffer.setData((const char *)m_shared->constData(), m_shared->size());

  m_buffer.open(QBuffer::ReadOnly);
  QDataStream in(&m_buffer);

  in >> m_Img;
  if (m_Img.isNull())
  {
    ret = Error("Image is null");
  }
  m_buffer.close();
  m_shared->unlock();

  return ret;
}

ICaptureImplementation::RetCode CaptureSharedmem::Close()
{
  if (m_shared)
    m_shared->detach();

  m_opened = false;
  return ICaptureImplementation::RetCode::OK;
}

QVideoFrame CaptureSharedmem::GetFrame()
{
  return QVideoFrame(m_Img);
}
