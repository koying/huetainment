#ifndef CaptureDemoYUV_H
#define CaptureDemoYUV_H

#include <QObject>
#include <QImage>

#include "ICaptureImplementation.h"

class CaptureDemoYUV : public QObject, public ICaptureImplementation
{
  Q_OBJECT

public:
  explicit CaptureDemoYUV(QObject *parent = nullptr);
  ~CaptureDemoYUV();

  // ICaptureImplementation interface
public:
  ICaptureImplementation::RetCode Open() override;
  ICaptureImplementation::RetCode RequestNewFrame() override;
  ICaptureImplementation::RetCode Close() override;
  QVideoFrame GetFrame() override;
};

#endif // CaptureDemoYUV_H
