#include "CaptureScreenshot.h"

#include <QScreen>
#include <QGuiApplication>
#include <QDebug>

CaptureScreenshot::CaptureScreenshot(QObject *parent)
  : QObject(parent)
{
  qInfo() << "Opening screenshot source";
}

CaptureScreenshot::~CaptureScreenshot()
{
}

ICaptureImplementation::RetCode CaptureScreenshot::Open()
{
  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureScreenshot::RequestNewFrame()
{
  QScreen *screen = QGuiApplication::primaryScreen();
  if (!screen)
    return Error("Cannot access primary screen");

  m_Pix = screen->grabWindow(0);
  qDebug() << "Screen grab: w:" << m_Pix.width() << "h:" << m_Pix.height();

  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureScreenshot::Close()
{
  return ICaptureImplementation::RetCode::OK;
}

QVideoFrame CaptureScreenshot::GetFrame()
{
  return QVideoFrame(m_Pix.toImage());
}