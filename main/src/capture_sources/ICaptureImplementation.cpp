#include "ICaptureImplementation.h"

QString ICaptureImplementation::device() const
{
  return m_device;
}

void ICaptureImplementation::setDevice(const QString& device)
{
  m_device = device;
}

QString ICaptureImplementation::videoStandard() const
{
  return m_videoStandard;
}

void ICaptureImplementation::setVideoStandard(const QString& videoStandard)
{
  m_videoStandard = videoStandard;
}
