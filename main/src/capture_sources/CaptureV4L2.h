#ifndef CAPTUREV4L2_H
#define CAPTUREV4L2_H

#include <QObject>

#include "ICaptureImplementation.h"

class V4L2VideoBuffer;

class CaptureV4L2 : public QObject, public ICaptureImplementation
{
  Q_OBJECT
public:
  explicit CaptureV4L2(QObject *parent = nullptr);
  ~CaptureV4L2() override;

signals:

public slots:

private:
  ICaptureImplementation::RetCode SetupCapture();
  ICaptureImplementation::RetCode StartStreaming();
  ICaptureImplementation::RetCode StopStreaming();

  int m_fd;
  unsigned int m_numBuffers;
  V4L2VideoBuffer** m_buffers;

  uint32_t m_xres;
  uint32_t m_yres;
  uint32_t m_rowstride;
  uint32_t m_pixelstride;
  QVideoFrame::PixelFormat m_format;

  QVideoFrame m_frame;

  // ICaptureImplementation interface
public:
  ICaptureImplementation::RetCode Open() override;
  ICaptureImplementation::RetCode RequestNewFrame() override;
  ICaptureImplementation::RetCode Close() override;
  QVideoFrame GetFrame() override;

  // ICaptureImplementation interface
public:
  void setDevice(const QString& device) override;
};

#endif // CAPTUREV4L2_H
