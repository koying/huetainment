#pragma once

#include <QObject>
#include <QImage>
#include <QPixmap>

#include "ICaptureImplementation.h"

class CaptureScreenshot : public QObject, public ICaptureImplementation
{
  Q_OBJECT

public:
  explicit CaptureScreenshot(QObject *parent = nullptr);
  ~CaptureScreenshot();

  // ICaptureImplementation interface
public:
  ICaptureImplementation::RetCode Open() override;
  ICaptureImplementation::RetCode RequestNewFrame() override;
  ICaptureImplementation::RetCode Close() override;
  QVideoFrame GetFrame() override;

private:
  QPixmap m_Pix;
};

