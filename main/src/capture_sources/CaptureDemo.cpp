#include "CaptureDemo.h"

CaptureDemo::CaptureDemo(QObject *parent)
  : QObject(parent)
{
  Q_INIT_RESOURCE(CaptureDemo);
}

CaptureDemo::~CaptureDemo()
{
  Q_CLEANUP_RESOURCE(CaptureDemo);
}

ICaptureImplementation::RetCode CaptureDemo::Open()
{
  m_demoImg = QImage(":/demo/demo.jpg");
  if (m_demoImg.isNull())
    return Error("Cannot load demo jpg");

  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureDemo::RequestNewFrame()
{
  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureDemo::Close()
{
  return ICaptureImplementation::RetCode::OK;
}

QVideoFrame CaptureDemo::GetFrame()
{
  return QVideoFrame(m_demoImg);
}