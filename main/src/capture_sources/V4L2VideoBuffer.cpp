#include "V4L2VideoBuffer.h"

#include <QVariant>
#include <QDebug>

#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

static int xioctl(int fh, unsigned long int request, void *arg)
{
  int r;

  do
  {
    r = ioctl(fh, request, arg);
  } while (-1 == r && EINTR == errno);

  return r;
}

V4L2VideoBuffer::V4L2VideoBuffer()
  : QAbstractVideoBuffer(QAbstractVideoBuffer::UserHandle)
  , m_fd(-1)
  , m_index(-1)
  , m_rowstride(0)
  , m_data(nullptr)
  , m_size(0)
{
}

bool V4L2VideoBuffer::error(QString msg)
{
  m_error = msg;
  return false;
}

bool V4L2VideoBuffer::allocate(int fd, int index, int rowstride)
{
  m_fd = fd;
  m_index = index;
  m_rowstride = rowstride;

  memset(&m_v4l2buf, 0, sizeof(m_v4l2buf));

  m_v4l2buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  m_v4l2buf.memory      = V4L2_MEMORY_MMAP;
  m_v4l2buf.index       = m_index;

  if (-1 == xioctl(m_fd, VIDIOC_QUERYBUF, &m_v4l2buf))
    return error("VIDIOC_QUERYBUF");

  m_size = m_v4l2buf.length;
  m_data =
      mmap(nullptr /* start anywhere */,
           m_v4l2buf.length,
           PROT_READ | PROT_WRITE /* required */,
           MAP_SHARED /* recommended */,
           m_fd, m_v4l2buf.m.offset);

  if (MAP_FAILED == m_data)
    return error("Cannot mmap");

  if (-1 == xioctl(m_fd, VIDIOC_QBUF, &m_v4l2buf))
    return error("VIDIOC_QBUF");

  return true;
}

bool V4L2VideoBuffer::free()
{
  if (!m_data || !m_size)
    return error("free: invalid buffer state");

  if (-1 == munmap(m_data, m_size))
    return error("Cannot munmap buffer");

  return true;
}

QVariant V4L2VideoBuffer::handle() const
{
  return static_cast<int>(m_index);
}

QAbstractVideoBuffer::HandleType V4L2VideoBuffer::handleType() const
{
  return QAbstractVideoBuffer::UserHandle;
}

QAbstractVideoBuffer::MapMode V4L2VideoBuffer::mapMode() const
{
  return QAbstractVideoBuffer::ReadOnly;
}

uchar *V4L2VideoBuffer::map(QAbstractVideoBuffer::MapMode mode, int *numBytes, int *bytesPerLine)
{
  if (mode != QAbstractVideoBuffer::ReadOnly)
    return nullptr;

  *numBytes = m_size;
  *bytesPerLine = m_rowstride;
  return static_cast<uchar*>(m_data);
}

void V4L2VideoBuffer::unmap()
{
}

void V4L2VideoBuffer::release()
{
  if (-1 == xioctl(m_fd, VIDIOC_QBUF, &m_v4l2buf))
    qWarning() << "release: Cannot enqueue buffer";
}
