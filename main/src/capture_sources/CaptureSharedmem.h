#pragma once

#include <QObject>
#include <QImage>
#include <QSharedMemory>
#include <QBuffer>

#include "ICaptureImplementation.h"

class CaptureSharedmem : public QObject, public ICaptureImplementation
{
  Q_OBJECT

public:
  explicit CaptureSharedmem(QObject *parent = nullptr);
  ~CaptureSharedmem();

  // ICaptureImplementation interface
public:
  ICaptureImplementation::RetCode Open() override;
  ICaptureImplementation::RetCode RequestNewFrame() override;
  ICaptureImplementation::RetCode Close() override;
  QVideoFrame GetFrame() override;

private:
  bool m_opened = false;
  QBuffer m_buffer;
  QSharedMemory* m_shared = nullptr;
  QImage m_Img;
};

