#include "CaptureDemoYUV.h"

#include <QFile>

CaptureDemoYUV::CaptureDemoYUV(QObject *parent)
  : QObject(parent)
{
  Q_INIT_RESOURCE(CaptureDemo);
}

CaptureDemoYUV::~CaptureDemoYUV()
{
  Q_CLEANUP_RESOURCE(CaptureDemo);
}

ICaptureImplementation::RetCode CaptureDemoYUV::Open()
{
  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureDemoYUV::RequestNewFrame()
{

  return ICaptureImplementation::RetCode::OK;
}

ICaptureImplementation::RetCode CaptureDemoYUV::Close()
{
  return ICaptureImplementation::RetCode::OK;
}

QVideoFrame CaptureDemoYUV::GetFrame()
{
  QFile f(":/demo/frame.raw");
  qint64 sz = f.size();

  QVideoFrame frame(sz, QSize(720, 480), 1440, QVideoFrame::Format_YUYV);
  frame.map(QAbstractVideoBuffer::WriteOnly);

  f.open(QIODevice::ReadOnly);
  f.read((char*)frame.bits(), sz);
  f.close();

  frame.unmap();

  return frame;
}
