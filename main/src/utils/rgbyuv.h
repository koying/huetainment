/*
#             (C) 2008 Hans de Goede <hdegoede@redhat.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __LIBconvert_PRIV_H
#define __LIBconvert_PRIV_H

void convert_rgb24_to_yuv420(const unsigned char *src, unsigned char *dest,
  int width, int height, int bytesperline, int bgr, int yvu);

void convert_yuv420_to_rgb24(const unsigned char *src, unsigned char *dst,
  int width, int height, int yvu);

void convert_yuv420_to_bgr24(const unsigned char *src, unsigned char *dst,
  int width, int height, int yvu);

void convert_yuyv_to_rgb24(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_yuyv_to_bgr24(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_yuyv_to_yuv420(const unsigned char *src, unsigned char *dst,
  int width, int height, int yvu);

void convert_yvyu_to_rgb24(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_yvyu_to_bgr24(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_uyvy_to_rgb24(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_uyvy_to_bgr24(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_uyvy_to_yuv420(const unsigned char *src, unsigned char *dst,
  int width, int height, int yvu);

void convert_swap_rgb(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_swap_uv(const unsigned char *src, unsigned char *dst,
  int width, int height, int bytesperline);

void convert_rgb565_to_rgb24(const unsigned char *src, unsigned char *dest,
  int width, int height);

void convert_rgb565_to_bgr24(const unsigned char *src, unsigned char *dest,
  int width, int height);

void convert_rgb565_to_yuv420(const unsigned char *src, unsigned char *dest,
  int width, int height, int bytesperline, int yvu);

void convert_spca501_to_yuv420(const unsigned char *src, unsigned char *dst,
  int width, int height, int yvu);

void convert_spca505_to_yuv420(const unsigned char *src, unsigned char *dst,
  int width, int height, int yvu);

void convert_spca508_to_yuv420(const unsigned char *src, unsigned char *dst,
  int width, int height, int yvu);

void convert_sn9c20x_to_yuv420(const unsigned char *src, unsigned char *dst,
  int width, int height, int yvu);

void convert_decode_spca561(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_decode_sn9c10x(const unsigned char *src, unsigned char *dst,
  int width, int height);

int convert_decode_pac207(struct convert_data *data,
  const unsigned char *inp, int src_size, unsigned char *outp,
  int width, int height);

void convert_decode_mr97310a(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_decode_sn9c2028(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_decode_sq905c(const unsigned char *src, unsigned char *dst,
  int width, int height);

void convert_bayer_to_rgb24(const unsigned char *bayer,
  unsigned char *rgb, int width, int height, unsigned int pixfmt);

void convert_bayer_to_bgr24(const unsigned char *bayer,
  unsigned char *rgb, int width, int height, unsigned int pixfmt);

void convert_bayer_to_yuv420(const unsigned char *bayer, unsigned char *yuv,
  int width, int height, unsigned int src_pixfmt, int yvu);

void convert_hm12_to_rgb24(const unsigned char *src,
  unsigned char *dst, int width, int height);

void convert_hm12_to_bgr24(const unsigned char *src,
  unsigned char *dst, int width, int height);

void convert_hm12_to_yuv420(const unsigned char *src,
  unsigned char *dst, int width, int height, int yvu);

void convert_rotate90(unsigned char *src, unsigned char *dest,
  struct v4l2_format *fmt);

void convert_flip(unsigned char *src, unsigned char *dest,
  struct v4l2_format *fmt, int hflip, int vflip);

void convert_crop(unsigned char *src, unsigned char *dest,
  int width, int height, int bytesperline, const struct v4l2_format *dest_fmt);

int convert_helper_decompress(struct convert_data *data,
  const char *helper, const unsigned char *src, int src_size,
  unsigned char *dest, int dest_size, int width, int height, int command);

void convert_helper_cleanup(struct convert_data *data);

#endif
