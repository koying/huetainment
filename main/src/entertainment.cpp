#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QElapsedTimer>

#include "huetainment.h"
#include "huebridge.h"
#include "entertainment.h"
#include "utility.h"

#define READ_TIMEOUT_MS 1000
#define MAX_RETRY       5
#define DEBUG_LEVEL 2000
#define SERVER_PORT_INT 2100
#define SERVER_PORT "2100"
#define SERVER_NAME "Hue"

EntertainmentGroup::EntertainmentGroup(HueBridge *parent)
    : BridgeObject(parent),
    name(),
    lights(),
    isStreaming(false),
    eThread(nullptr)
{
    emit propertiesChanged();

    connect(qnam, SIGNAL(finished(QNetworkReply*)),
        this, SLOT(replied(QNetworkReply*)));
}

EntertainmentGroup::~EntertainmentGroup()
{
    if (eThread != nullptr)
    {
        eThread->stop();
        while (eThread->isRunning())
        {
            QThread::msleep(100);
        }
    }
}

void EntertainmentGroup::updateLightXZ(int index, float x, float z, bool save)
{
    lights[index].x = x;
    lights[index].z = z;

    if (eThread != nullptr) {
        EntertainmentCommThreadEGroupScopedLock g(eThread);
        g->updateLightXZ(index, x, z, false);
    }

    if (!save || bridgeParent() == nullptr) {
        return;
    }

    QNetworkRequest qnr = bridgeParent()->makeRequest(QString("/groups/%1").arg(id));
    qnr.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QJsonObject locations;
    for (EntertainmentLight& light : lights)
    {
        QJsonArray arr;
        arr.append(light.x);
        arr.append(light.y);
        arr.append(light.z);
        locations.insert(light.id, arr);
    }

    QJsonObject body;
    body.insert("locations", locations);

    qnam->put(qnr, QJsonDocument(body).toJson());
}

void EntertainmentGroup::startStreaming(GetColorFunction getColorFunc)
{
  getColor = getColorFunc;
  askBridgeToToggleStreaming(true);
}

void EntertainmentGroup::stopStreaming()
{
  if (eThread)
    eThread->stop();
  askBridgeToToggleStreaming(false);
}

void EntertainmentGroup::shutDownImmediately()
{
  if (eThread)
    eThread->stop();
}
bool EntertainmentGroup::hasRunningThread()
{
  if (eThread)
    return eThread->isRunning();

  return false;
}

void EntertainmentGroup::askBridgeToToggleStreaming(bool enable)
{
    QNetworkRequest qnr = bridgeParent()->makeRequest(QString("/groups/%1").arg(id));
    qnr.setOriginatingObject(this);
    qnr.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QJsonObject stream;
    stream.insert("active", enable);

    QJsonObject body;
    body.insert("stream", stream);

    qnam->put(qnr, QJsonDocument(body).toJson());
}

void EntertainmentGroup::entertainmentThreadConnected()
{
    isStreaming = true;
    emit isStreamingChanged(true);
}
void EntertainmentGroup::entertainmentThreadFinished()
{
    if (!isStreaming)
    {
        emit udpConnectFailed();
    }

    askBridgeToToggleStreaming(false);
}

void EntertainmentGroup::replied(QNetworkReply *reply)
{
  if (reply->request().originatingObject() != this)
    return;

  QByteArray data = reply->readAll();

  if (!isStreaming)
  {
    if (data.contains("false"))
      emit failedToStartStreaming();
    else if (data.contains("true"))
    {
      eThread = new EntertainmentCommThread(this, bridgeParent()->username, bridgeParent()->clientkey, bridgeParent()->address.toString(), *this, getColor);

      connect(eThread, SIGNAL(connected()),
              this, SLOT(entertainmentThreadConnected()));

      connect(eThread, SIGNAL(finished()),
              this, SLOT(entertainmentThreadFinished()));

      eThread->start();
    }
  }
  else
  {
    if (data.contains("false"))
    {
      while (eThread->isRunning())
        QThread::msleep(100);
      delete eThread;
      isStreaming = false;
      for (auto& light : lights)
        bridgeParent()->restoreLight(light.id);

      emit isStreamingChanged(false);
    }
  }
}

EntertainmentCommThread::EntertainmentCommThread(QObject *parent, QString inUsername, QString inClientkey, QString inAddress, const EntertainmentGroup& inEGroup, GetColorFunction getColorFunc)
    : QThread(parent),
    username(inUsername),
    clientkey(inClientkey),
    stopRequested(false),
    address(inAddress),
    crypto(QSslSocket::SslClientMode),
    eGroup(inEGroup),
    getColor(getColorFunc)
{
  //We make a local copy of the group and lights for thread safety, but
  //must prevent their being destroyed if inEGroup's parent is destroyed
  eGroup.setParent(nullptr);
  for (auto& light : eGroup.lights)
  {
    light.setParent(nullptr);
  }

  auto configuration = QSslConfiguration::defaultDtlsConfiguration();
  configuration.setPeerVerifyMode(QSslSocket::VerifyNone);
  crypto.setPeer(QHostAddress(address), SERVER_PORT_INT);
  crypto.setDtlsConfiguration(configuration);

  connect(&crypto, &QDtls::handshakeTimeout, this, &EntertainmentCommThread::handshakeTimeout);
  connect(&crypto, &QDtls::pskRequired, this, &EntertainmentCommThread::pskRequired);
  socket.connectToHost(address, SERVER_PORT_INT);
  connect(&socket, &QUdpSocket::readyRead, this, &EntertainmentCommThread::readyRead);

  startHandshake();
}

EntertainmentCommThread::~EntertainmentCommThread()
{
  qDebug() << "DTLS: Closing the connection...";
  if (crypto.isConnectionEncrypted())
      crypto.shutdown(&socket);
}

void EntertainmentCommThread::stop()
{
  stopRequested = true;
}

void EntertainmentCommThread::startHandshake()
{
  if (socket.state() != QAbstractSocket::ConnectedState)
  {
    qDebug() << "DTLS: connecting UDP socket first ...";
    connect(&socket, &QAbstractSocket::connected, this, &EntertainmentCommThread::udpSocketConnected);
    return;
  }

  if (!crypto.doHandshake(&socket))
  {
    qDebug() << tr("DTLS: failed to start a handshake - %1").arg(crypto.dtlsErrorString());
    emit connectFailed();
  }
  else
    qDebug() << "DTLS: starting a handshake";
}

void EntertainmentCommThread::udpSocketConnected()
{
  qDebug() << (tr("DTLS: UDP socket is now in ConnectedState, continue with handshake ..."));
  startHandshake();
}

void EntertainmentCommThread::readyRead()
{
  if (socket.pendingDatagramSize() <= 0)
  {
    qDebug() << (tr("DTLS: spurious read notification? "));
    return;
  }

  QByteArray dgram(socket.pendingDatagramSize(), Qt::Uninitialized);
  const qint64 bytesRead = socket.readDatagram(dgram.data(), dgram.size());
  if (bytesRead <= 0)
  {
    qDebug() << (tr("DTLS: spurious read notification?"));
    return;
  }

  dgram.resize(bytesRead);
  if (crypto.isConnectionEncrypted())
  {
    const QByteArray plainText = crypto.decryptDatagram(&socket, dgram);
    if (plainText.size())
    {
//      emit serverResponse("DTLS", dgram, plainText);
      return;
    }

    if (crypto.dtlsError() == QDtlsError::RemoteClosedConnectionError)
    {
      qDebug() << (tr("DTLS: shutdown alert received"));
      socket.close();
      stop();
      return;
    }

    qDebug() << (tr("DTLS: zero-length datagram received?"));
  }
  else
  {
    if (!crypto.doHandshake(&socket, dgram))
    {
      qDebug() << (tr("DTLS: handshake error - %1").arg(crypto.dtlsErrorString()));
      emit connectFailed();
      return;
    }

    if (crypto.isConnectionEncrypted())
    {
      qDebug() << (tr("DTLS: encrypted connection established!"));
      emit connected();
    }
    else
    {
      qDebug() << (tr("DTLS: continuing with handshake ..."));
    }
  }
}

void EntertainmentCommThread::handshakeTimeout()
{
  qDebug() << (tr("DTLS: handshake timeout, trying to re-transmit"));
  if (!crypto.handleTimeout(&socket))
  {
    qDebug() << (tr("DTLS: failed to re-transmit - %1").arg( crypto.dtlsErrorString()));
    emit connectFailed();
  }
}

void EntertainmentCommThread::pskRequired(QSslPreSharedKeyAuthenticator *auth)
{
  Q_ASSERT(auth);

  qDebug() << (tr("DTLS: providing pre-shared key ..."));
  auth->setIdentity(username.toLatin1());
  auth->setPreSharedKey(QByteArray::fromHex(clientkey.toLatin1()));
}

void EntertainmentCommThread::run()
{
  while (!stopRequested)
  {
    if (!crypto.isConnectionEncrypted())
    {
      QThread::msleep(30);
      continue;
    }

    static const uint8_t HEADER[] = {
      'H', 'u', 'e', 'S', 't', 'r', 'e', 'a', 'm', //protocol
      0x01, 0x00, //version 1.0
      0x01, //sequence number 1
      0x00, 0x00, //Reserved write 0’s
      0x01, // 0x00 = RGB; 0x01 = XY Brightness
      0x00, // Reserved, write 0’s
    };

    static const uint8_t PAYLOAD_PER_LIGHT[] =
    {
      0x01, 0x00, 0x06, //light ID

      //color: 16 bpc
      0xff, 0xff,
      0xff, 0xff,
      0xff, 0xff,
      /*
                              (message.R >> 8) & 0xff, message.R & 0xff,
                              (message.G >> 8) & 0xff, message.G & 0xff,
                              (message.B >> 8) & 0xff, message.B & 0xff
                              */
    };

    QByteArray Msg;

    eGroupMutex.lock();
    Msg.reserve(sizeof(HEADER) + sizeof(PAYLOAD_PER_LIGHT) * eGroup.lights.size());
    Msg.append((char*)HEADER, sizeof(HEADER));

    static QElapsedTimer timer;
    double deltaTime = timer.restart() / 1000.0;
    if (deltaTime > 1.0 || deltaTime <= 0.0 || std::isnan(deltaTime)) {
      deltaTime = 1.0;
    }

    for (auto& light : eGroup.lights)
    {
      double new_x = 0.0;
      double new_y = 0.0;
      double new_b = 0.0;

      double minBrightness, maxBrightness, brightnessBoost;

      if (!getColor(light, new_x, new_y, new_b, deltaTime))
        continue;

      light.cx = new_x;
      light.cy = new_y;
      light.cb = new_b;

      double x = new_x;
      double y = new_y;
      Color::FitInGamut(x, y);

      quint16 oX = x * 0xffff;
      quint16 oY = y * 0xffff;
      quint16 oB = light.cb * 0xffff;

      const uint8_t payload[] = {
        0x00, 0x00, ((uint8_t)light.id.toInt()),

        static_cast<uint8_t>((oX >> 8) & 0xff), static_cast<uint8_t>(oX & 0xff),
        static_cast<uint8_t>((oY >> 8) & 0xff), static_cast<uint8_t>(oY & 0xff),
        static_cast<uint8_t>((oB >> 8) & 0xff), static_cast<uint8_t>(oB & 0xff)
      };

      Msg.append((char*)payload, sizeof(payload));
    }
    eGroupMutex.unlock();

    const qint64 written = crypto.writeDatagramEncrypted(&socket, Msg);
    if (written <= 0) {
      qCritical() << (tr("DTLS: failed to send message: %1").arg(crypto.dtlsErrorString()));
      break;
    }

    //TODO: make this delay customizable?
    QThread::msleep(15);
  }
}
