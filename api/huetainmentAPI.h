#pragma once
#include <Cutelyst/Application>

using namespace Cutelyst;

class HuetainmentAPI : public Application
{
    Q_OBJECT
    CUTELYST_APPLICATION(IID "Huetainment")
public:
    Q_INVOKABLE explicit HuetainmentAPI(QObject *parent = nullptr);
    ~HuetainmentAPI();

    bool init();
};

