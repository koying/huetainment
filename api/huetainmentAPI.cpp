#include "huetainmentAPI.h"

#include "apiv1.h"

using namespace Cutelyst;

HuetainmentAPI::HuetainmentAPI(QObject *parent) : Application(parent)
{
}

HuetainmentAPI::~HuetainmentAPI()
{
}

bool HuetainmentAPI::init()
{
  new ApiV1 (this);

  return true;
}

