#include "apiv1.h"

#include "main/include/huetainment.h"
#include "main/include/bridgediscovery.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTimer>
#include <QImage>
#include <QByteArray>
#include <QBuffer>

using namespace Cutelyst;

ApiV1::ApiV1(QObject *parent)
  : Controller(parent)
  , m_pairingInProgress(false)
{
  m_huetainment = new Huetainment(this);
}

ApiV1::~ApiV1()
{
}

void ApiV1::states(Context *c)
{
  qDebug() << Q_FUNC_INFO;
}

void ApiV1::states_GET(Context *c)
{
  qDebug() << Q_FUNC_INFO;


  c->response()->setJsonObjectBody({
                                     {QStringLiteral("capturing"), m_huetainment->property("capturing").toBool()},
                                     {QStringLiteral("syncing"), m_huetainment->property("syncing").toBool()},
                                   });
}

void ApiV1::states_PUT(Context *c)
{
  qDebug() << Q_FUNC_INFO;

  const QJsonDocument doc = c->request()->bodyData().toJsonDocument();
  const QJsonObject obj = doc.object();

  QString capdevice;
  if (obj.contains(QStringLiteral("captureDevice")))
    capdevice = obj.value(QStringLiteral("captureDevice")).toString();
  QString syncgroup;
  if (obj.contains(QStringLiteral("syncingGroup")))
   syncgroup = obj.value(QStringLiteral("syncingGroup")).toString();

  m_huetainment->WriteSettings();

  if (obj.contains(QStringLiteral("syncing")))
  {
    bool setsyncing = obj.value(QStringLiteral("syncing")).toBool();
    if (setsyncing != m_huetainment->property("syncing").toBool())
    {
      setsyncing ? m_huetainment->startScreenSync(capdevice, syncgroup) : m_huetainment->stopScreenSync();
    }
  }
  if (obj.contains(QStringLiteral("capturing")))
  {
    bool setcapture = obj.value(QStringLiteral("capturing")).toBool();
    if (setcapture != m_huetainment->property("capturing").toBool())
    {
      if (setcapture)
      {
        if (!m_huetainment->startCapturing())
        {
          c->response()->setJsonObjectBody({
                                         {QStringLiteral("error"), m_huetainment->getError()},
                                     });
          return;
        }
      }
      else
      {
        m_huetainment->stopCapturing();
      }
    }
  }

  c->response()->setJsonObjectBody({
                                     {QStringLiteral("capturing"), m_huetainment->property("capturing").toBool()},
                                     {QStringLiteral("syncing"), m_huetainment->property("syncing").toBool()},
                                   });
}

void ApiV1::settings(Context *c)
{
  qDebug() << Q_FUNC_INFO;
}

void ApiV1::settings_GET(Context *c)
{
  qDebug() << Q_FUNC_INFO;

  QRect r = m_huetainment->property("captureMargins").toRect();
  QJsonArray ra = { r.x(), r.y(), r.width(), r.height()};

  c->response()->setJsonObjectBody({
                                     {QStringLiteral("captureDevice"), m_huetainment->property("captureDevice").toString()},
                                     {QStringLiteral("captureVideoStandard"), m_huetainment->property("captureVideoStandard").toString()},
                                     {QStringLiteral("captureMargins"), ra},
                                     {QStringLiteral("captureMultiplier"),  m_huetainment->property("captureMultiplier").toInt()},
                                     {QStringLiteral("syncingGroup"), m_huetainment->property("syncingGroup").toString()},
                                     {QStringLiteral("captureInterval"), m_huetainment->getCaptureInterval()},
                                     {QStringLiteral("maxLuminance"), m_huetainment->getMaxLuminance() * 100},
                                     {QStringLiteral("minLuminance"), m_huetainment->getMinLuminance() * 100},
                                     {QStringLiteral("chromaBoost"), m_huetainment->getChromaBoost() * 100},
                                     {QStringLiteral("lumaBoost"), m_huetainment->getLumaBoost() * 100},
                                     {QStringLiteral("colorBias"), m_huetainment->getColorBias()},
                                     {QStringLiteral("centerSlowness"), m_huetainment->getCenterSlowness() * 100},
                                     {QStringLiteral("sideSlowness"), m_huetainment->getSideSlowness() * 100},
                                     {QStringLiteral("satThreshold"), m_huetainment->getSatThreshold() * 100},
                                     {QStringLiteral("valThreshold"), m_huetainment->getValThreshold() * 100},
                                   });

}

void ApiV1::settings_PUT(Context *c)
{
  qDebug() << Q_FUNC_INFO;

  const QJsonDocument doc = c->request()->bodyData().toJsonDocument();
  const QJsonObject obj = doc.object();

  if (obj.contains(QStringLiteral("captureDevice")))
    m_huetainment->setProperty("captureDevice", obj.value(QStringLiteral("captureDevice")).toString());
  if (obj.contains(QStringLiteral("captureVideoStandard")))
    m_huetainment->setProperty("captureVideoStandard", obj.value(QStringLiteral("captureVideoStandard")).toString());
  if (obj.contains(QStringLiteral("captureMargins")))
  {
    if (obj.value(QStringLiteral("captureMargins")).isArray())
    {
      QJsonArray ja = obj.value(QStringLiteral("captureMargins")).toArray();
      m_huetainment->setProperty("captureMargins", QRect(ja[0].toInt(), ja[1].toInt(), ja[2].toInt(), ja[3].toInt()));
    }
  }
  if (obj.contains(QStringLiteral("captureMultiplier")))
    m_huetainment->setProperty("captureMultiplier", obj.value(QStringLiteral("captureMultiplier")).toInt());
  if (obj.contains(QStringLiteral("syncingGroup")))
    m_huetainment->setProperty("syncingGroup", obj.value(QStringLiteral("syncingGroup")).toString());
  if (obj.contains(QStringLiteral("captureInterval")))
    m_huetainment->setCaptureInterval(obj.value(QStringLiteral("captureInterval")).toInt());
  if (obj.contains(QStringLiteral("maxLuminance")))
    m_huetainment->setMaxLuminance(obj.value(QStringLiteral("maxLuminance")).toDouble() / 100.0);
  if (obj.contains(QStringLiteral("minLuminance")))
    m_huetainment->setMinLuminance(obj.value(QStringLiteral("minLuminance")).toDouble() / 100.0);
  if (obj.contains(QStringLiteral("chromaBoost")))
    m_huetainment->setChromaBoost(obj.value(QStringLiteral("chromaBoost")).toDouble() / 100.0);
  if (obj.contains(QStringLiteral("lumaBoost")))
    m_huetainment->setLumaBoost(obj.value(QStringLiteral("lumaBoost")).toDouble() / 100.0);
  if (obj.contains(QStringLiteral("colorBias")))
    m_huetainment->setColorBias(obj.value(QStringLiteral("colorBias")).toDouble());
  if (obj.contains(QStringLiteral("centerSlowness")))
    m_huetainment->setCenterSlowness(obj.value(QStringLiteral("centerSlowness")).toDouble() / 100.0);
  if (obj.contains(QStringLiteral("sideSlowness")))
    m_huetainment->setSideSlowness(obj.value(QStringLiteral("sideSlowness")).toDouble() / 100.0);
  if (obj.contains(QStringLiteral("satThreshold")))
    m_huetainment->setSatThreshold(obj.value(QStringLiteral("satThreshold")).toDouble() / 100.0);
  if (obj.contains(QStringLiteral("valThreshold")))
    m_huetainment->setValThreshold(obj.value(QStringLiteral("valThreshold")).toDouble() / 100.0);

  m_huetainment->WriteSettings();

  c->response()->setJsonObjectBody({
                                 {QStringLiteral("status"), QStringLiteral("ok")},
                             });

}

void ApiV1::bridges(Context *c)
{
  qDebug() << Q_FUNC_INFO;
}

void ApiV1::bridges_GET(Context *c)
{
  qDebug() << Q_FUNC_INFO;

  QJsonArray array;
  BridgeDiscovery* bridges = m_huetainment->getBridgeDiscovery();
  for (QObject* Obj : bridges->getModel())
  {
    HueBridge* bridge = qobject_cast<HueBridge*>(Obj);
    if (bridge != nullptr)
    {
      QJsonObject o({
                      {QStringLiteral("id"), bridge->id},
                      {QStringLiteral("friendlyName"), bridge->friendlyName},
                      {QStringLiteral("address"), bridge->getAddress()},
                      {QStringLiteral("model"), bridge->modelNumber},
                      {QStringLiteral("connected"), bridge->connected},
                      {QStringLiteral("needslink"), bridge->wantsLinkButton},
                    });
      if (!bridge->wantsLinkButton)
      {
        o.insert(QStringLiteral("username"), bridge->username);
        o.insert(QStringLiteral("clientkey"), bridge->clientkey);
      }
      array.append(o);
    }
  }
  c->response()->setJsonArrayBody(array);
}

void ApiV1::bridge(Context *c, const QString& id)
{
  qDebug() << Q_FUNC_INFO;
}

void ApiV1::bridge_POST(Context *c, const QString& id)
{
  BridgeDiscovery* bridges = m_huetainment->getBridgeDiscovery();
  for (QObject* Obj : bridges->getModel())
  {
    HueBridge* bridge = qobject_cast<HueBridge*>(Obj);
    if (bridge != nullptr && !bridge->connected && bridge->id == id)
    {
      m_pairingBridge = bridge;
      connect(m_pairingBridge, SIGNAL(connectedChanged()), this, SLOT(onBridgeConnected()));
      m_pairingTimer = new QTimer(this);
      connect(m_pairingTimer, SIGNAL(timeout()), this, SLOT(onPairingTick()));
      m_pairingTimer->start(2000);
      m_pairingInProgress = true;
      QTimer::singleShot(20000, this, SLOT(onPairingTimeout()));
    }
  }

  c->response()->setJsonObjectBody({
                                 {QStringLiteral("status"), QStringLiteral("Pairing initiated. Press the link button on the bridge.")},
                             });
}

void ApiV1::groups(Context *c)
{
  qDebug() << Q_FUNC_INFO;
}

void ApiV1::groups_GET(Context *c)
{
  qDebug() << Q_FUNC_INFO;

  QJsonArray array;
  m_huetainment->refreshGroups();
  QList<QObject*> groups = m_huetainment->getEntertainmentGroupsModel();
  for (QObject* Obj : groups)
  {
    QJsonObject o({
                    {QStringLiteral("name"), Obj->property("name").toString()},
                    {QStringLiteral("is_streaming"), Obj->property("isStreaming").toBool()},
                  });
    array.append(o);
  }
  c->response()->setJsonArrayBody(array);
}

void ApiV1::screenshot(Context *c)
{
  qDebug() << Q_FUNC_INFO;
}

void ApiV1::screenshot_GET(Context *c)
{
  qDebug() << Q_FUNC_INFO;

  if (!m_huetainment->property("capturing").toBool())
  {
    c->response()->setJsonObjectBody({
                                   {QStringLiteral("error"), QStringLiteral("Not currently capturing.")},
                               });
    return;
  }

  QImage img = m_huetainment->getImage(1024);

  QByteArray ba;
  QBuffer buf(&ba);
  buf.open(QIODevice::WriteOnly);
  img.save(&buf, "PNG");
  buf.close();

  c->response()->setContentType(QStringLiteral("image/png"));
  c->response()->setBody(ba);
}

void ApiV1::stats(Context* c)
{
  qDebug() << Q_FUNC_INFO;
}

void ApiV1::stats_GET(Context* c)
{
  qDebug() << Q_FUNC_INFO;

  int averageFrameRead = 0;
  if (m_huetainment->getFrameReads())
    averageFrameRead = m_huetainment->getTotalFrameReadElapsed() / m_huetainment->getFrameReads();

  QSize sz = m_huetainment->getLastFrameSize();
  QJsonArray sza = { sz.width(), sz.height() };

  c->response()->setJsonObjectBody({
                                     {QStringLiteral("lastFrameSize"), sza},
                                     {QStringLiteral("lastFrameRead"), static_cast<qint64>(m_huetainment->getLastFrameReadElapsed())},
                                     {QStringLiteral("averageFrameRead"), averageFrameRead},
                                   });
}

void ApiV1::onPairingTick()
{
  qDebug() << Q_FUNC_INFO;

  m_pairingBridge->connectToBridge();
}

void ApiV1::onPairingTimeout()
{
  qDebug() << Q_FUNC_INFO;

  if(m_pairingInProgress)
  {
    disconnect(m_pairingBridge, SIGNAL(connectedChanged()));
    m_pairingTimer->stop();
    m_pairingTimer->deleteLater();
    m_pairingTimer = nullptr;
    m_pairingInProgress = false;
  }
}

void ApiV1::onBridgeConnected()
{
  qDebug() << Q_FUNC_INFO;

  if (m_pairingBridge->connected)
  {
    onPairingTimeout();
  }
}

