#ifndef APIV1_H
#define APIV1_H

#include <Cutelyst/Controller>

class Huetainment;
class HueBridge;
class QTimer;

using namespace Cutelyst;

class ApiV1 : public Controller
{
  Q_OBJECT

public:
  explicit ApiV1(QObject *parent = nullptr);
  ~ApiV1();

  C_ATTR(states, :Local :AutoArgs :ActionClass(REST))
  void states(Context *c);

  C_ATTR(states_GET, :Private)
  void states_GET(Context *c);

  C_ATTR(states_PUT, :Private)
  void states_PUT(Context *c);

  C_ATTR(settings, :Local :AutoArgs :ActionClass(REST))
  void settings(Context *c);

  C_ATTR(settings_GET, :Private)
  void settings_GET(Context *c);

  C_ATTR(settings_PUT, :Private)
  void settings_PUT(Context *c);

  C_ATTR(bridges, :Local :AutoArgs :ActionClass(REST))
  void bridges(Context *c);

  C_ATTR(bridges_GET, :Private)
  void bridges_GET(Context *c);

  C_ATTR(bridge, :Local :AutoArgs :ActionClass(REST))
  void bridge(Context *c, const QString& id);

  C_ATTR(bridge_POST, :Private )
  void bridge_POST(Context *c, const QString& id);

  C_ATTR(groups, :Local :AutoArgs :ActionClass(REST))
  void groups(Context *c);

  C_ATTR(groups_GET, :Private)
  void groups_GET(Context *c);

  C_ATTR(screenshot, :Local :AutoArgs :ActionClass(REST))
  void screenshot(Context *c);

  C_ATTR(screenshot_GET, :Private)
  void screenshot_GET(Context *c);

  C_ATTR(stats, :Local :AutoArgs :ActionClass(REST))
  void stats(Context *c);

  C_ATTR(stats_GET, :Private)
  void stats_GET(Context *c);

private:
  Huetainment *m_huetainment;
  bool m_pairingInProgress;
  QTimer* m_pairingTimer;
  HueBridge* m_pairingBridge;

private slots:
  void onPairingTick();
  void onPairingTimeout();
  void onBridgeConnected();
};

#endif //APIV1_H

